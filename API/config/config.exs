# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :typetype,
  ecto_repos: [Typetype.Repo]

# Configures the endpoint
config :typetype, TypetypeWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "r7cjVKbnhsW9M3fr7E3tGs1T7bEI/KMaMfoufk7RFY4B2lWqBSMKX1zMuUmjRO11",
  render_errors: [view: TypetypeWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Typetype.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
