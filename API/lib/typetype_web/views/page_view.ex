defmodule TypetypeWeb.PageView do
  use TypetypeWeb, :view

  def current_quote do
    "If you prick us, do we not bleed? If you tickle us, do we not laugh? If you poison us, do we not die? And if you wrong us, shall we not revenge?"
  end
end
