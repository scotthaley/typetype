defmodule TypetypeWeb.ComponentHelpers do
  def component(template, assigns \\ []) do
    TypetypeWeb.ComponentView.render(template, assigns)
  end

  def component(template, assigns, do: block) do
    TypetypeWeb.ComponentView.render(template, Keyword.merge(assigns, [do: block]))
  end
end
