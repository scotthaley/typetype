defmodule TypetypeWeb.Router do
  use TypetypeWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TypetypeWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/new-match", PageController, :new_match
    get "/match/:id", PageController, :match
    get "/practice", PageController, :practice
    get "/matches", MatchController, :active_matches
  end

  # Other scopes may use custom stacks.
  # scope "/api", TypetypeWeb do
  #   pipe_through :api
  # end
end
