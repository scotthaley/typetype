defmodule TypetypeWeb.MatchController do
  use TypetypeWeb, :controller

  alias Typetype.{Repo, Match, MatchState}
  import Ecto.Query, only: [from: 2]

  def active_matches(conn, _params) do
    matches = MatchState.get_matches
    json(conn, %{matches: matches})
  end

  defp match_data(match) do
    match_status = MatchState.get_match_status(Integer.to_string(match.id))
    %{
      id: match.id,
      players: match_status.players,
      ready: match_status.ready,
      started: match_status.started
    }
  end
end
