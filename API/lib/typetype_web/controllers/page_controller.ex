defmodule TypetypeWeb.PageController do
  use TypetypeWeb, :controller

  alias Typetype.{Repo, Match, MatchState}

  def index(conn, _params) do
    render(conn, "new-match.html")
  end

  def new_match(conn, _params) do
    {:ok, match} = Repo.insert(%Match{})
    conn |> redirect(to: "/match/#{match.id}") |> halt()
  end

  def match(conn, %{"id" => id}) do
    match = Repo.get(Match, id)
    matchStatus = MatchState.get_match_status(id)
    # TODO: check if match has already been completed
    render(conn, "match.html", [matchId: match.id, matchStatus: matchStatus])
  end

  def practice(conn, _params) do
    render(conn, "practice.html")
  end
end
