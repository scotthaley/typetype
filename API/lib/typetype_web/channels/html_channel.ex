defmodule TypetypeWeb.HtmlChannel do
  use Phoenix.Channel

  def join("html", message, socket) do
    {:ok, socket}
  end

  def join("html:" <> _match_id, message, socket) do
    {:ok, socket}
  end

  def broadcast_html(html, container, match_id) do
    payload = %{
      "container" => container,
      "html" => html
    }

    TypetypeWeb.Endpoint.broadcast_from!(self(), "html:" <> match_id, "update", payload)
  end
end