defmodule TypetypeWeb.PlayerChannel do
  use Phoenix.Channel
  require Logger

  alias Typetype.MatchState

  def join("players:" <> _private_room_id, message, socket) do
    match_id = socket.assigns.match_id
    players = MatchState.players(match_id)
    send(self, {:after_join, message})

    {:ok, %{players: players}, socket}
  end

  def handle_info({:after_join, _message}, socket) do
    player_name = socket.assigns.player_name
    match_id = socket.assigns.match_id
    player = MatchState.put_player(match_id, player_name)
    push(socket, "match_status", MatchState.get_match_status(match_id))
    push(socket, "self_update", player)
    {:noreply, socket}
  end

  def handle_in("player:ready", _payload, socket) do
    player_name = socket.assigns.player_name
    match_id = socket.assigns.match_id
    player = MatchState.player_ready(match_id, player_name)
    broadcast!(socket, "match_status", MatchState.get_match_status(match_id))
    push(socket, "self_update", player)
    {:noreply, socket}
  end

  def handle_in("player:fetch", _payload, socket) do
    player_name = socket.assigns.player_name
    match_id = socket.assigns.match_id
    push(socket, "self_update", MatchState.get_player(match_id, player_name))
    {:noreply, socket}
  end

  def handle_in("player:input", payload, socket) do
    player_name = socket.assigns.player_name
    match_id = socket.assigns.match_id
    player = MatchState.player_input(match_id, player_name, payload["input"])
    push(socket, "self_update", player)
    {:noreply, socket}
  end

  def handle_in("player:backspace", payload, socket) do
    player_name = socket.assigns.player_name
    match_id = socket.assigns.match_id
    player = MatchState.player_backspace(match_id, player_name)
    push(socket, "self_update", player)
    {:noreply, socket}
  end

  def handle_in("player:reset_match", payload, socket) do
    match_id = socket.assigns.match_id
    MatchState.reset_match(match_id)
    broadcast!(socket, "match_status", MatchState.get_match_status(match_id))
    broadcast!(socket, "match_reset", %{})
    {:noreply, socket}
  end

  def handle_in("player:category_vote", payload, socket) do
    player_name = socket.assigns.player_name
    match_id = socket.assigns.match_id
    player = MatchState.player_vote(match_id, player_name, payload["name"])
    push(socket, "self_update", player)
    broadcast!(socket, "match_status", MatchState.get_match_status(match_id))
    {:noreply, socket}
  end

  def terminate(reason, socket) do
    match_id = socket.assigns.match_id
    player_name = socket.assigns.player_name
    MatchState.remove_player(match_id, player_name)
  end

  def broadcast_match_status(match_id, match) do
    TypetypeWeb.Endpoint.broadcast_from!(self(), "players:" <> match_id, "match_status", match)
  end
end
