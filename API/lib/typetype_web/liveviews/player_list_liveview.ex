defmodule TypetypeWeb.LiveView.PlayerList do
  use TypetypeWeb.LiveView
  alias Typetype.MatchState
  require Logger

  def container, do: "#player-list"
  def template, do: "player-list.html"
  def assigns(params) do 
    [players: Map.values(MatchState.players(params[:match_id]))]
  end
end