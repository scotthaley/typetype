defmodule TypetypeWeb.LiveView do
  @callback template() :: String.t
  @callback container() :: String.t

  defmacro __using__(_params) do
    quote do
      @behavior TypetypeWeb.LiveView

      def view, do: TypetypeWeb.ComponentView
      def assigns, do: []
      def assigns(params), do: params
      def render(), do: Phoenix.View.render_to_string(view(), template(), assigns())
      def render(params), do: Phoenix.View.render_to_string(view(), template(), assigns(params))
      def update(room), do: TypetypeWeb.HtmlChannel.broadcast_html(render(), container(), room)
      def update(params, room), do: TypetypeWeb.HtmlChannel.broadcast_html(render(params), container(), room)

      defoverridable [view: 0, assigns: 0, assigns: 1]
    end
  end
end