defmodule TypetypeWeb.LiveView.MatchStatus do
  use TypetypeWeb.LiveView
  alias Typetype.MatchState

  def container, do: "#match-status"
  def template, do: "match-status.html"
  def assigns(params) do 
    [matchStatus: MatchState.get_match_status(params[:match_id])]
  end
end