defmodule TypetypeWeb.LiveView.MatchReadyup do
  use TypetypeWeb.LiveView
  alias Typetype.MatchState

  def container, do: "#match-input"
  def template, do: "match-readyup.html"
end