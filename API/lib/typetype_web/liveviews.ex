defmodule TypetypeWeb.LiveViews do
  alias TypetypeWeb.LiveView

  def update_view(id, room), do: live_view(id).update(room)
  def update_view(id, params, room), do: live_view(id).update(params, room)
  def update_view(id, params, room, player), do: live_view(id).update(params, "#{room}:#{player}")

  def live_view(:player_list), do: LiveView.PlayerList
  def live_view(:readyup), do: LiveView.MatchReadyup
  def live_view(:match_status), do: LiveView.MatchStatus
end