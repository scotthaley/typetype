defmodule Typetype.MatchBroadcaster do
  use GenServer
  require Logger

  alias Typetype.MatchState
  alias TypetypeWeb.PlayerChannel

  def start_link(_default) do
    GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
  end

  def init(state) do
    Process.send_after(self(), :tick, 1000)
    {:ok, state}
  end

  def handle_info(:tick, state) do
    broadcast_match_status()
    Process.send_after(self(), :tick, 1000)
    {:noreply, state}
  end

  defp broadcast_match_status do
    MatchState.tick_all_matches()
    MatchState.get_matches()
    |> Enum.each(&PlayerChannel.broadcast_match_status(&1.id, &1))
  end

end