defmodule Typetype.MatchState do
  require Logger

  @moduledoc """
  This module holds the current match state.
  """

  alias Typetype.Quotes

  @match_limit 120
  @countdown 10
  @player_ready_min 3

  @doc """
  Used by the supervisor to start the Agent that will keep the game state persistent.
  The initial value passed to the Agent is an empty map.
  """
  def start_link do
    Agent.start_link(fn -> %{} end, name: __MODULE__)
  end

  @doc """
  Get list of matches
  """
  def get_matches do
    Agent.get(__MODULE__, &payloadify_matches(&1))
  end

  defp is_match_active(match) do
    match.players |> Map.keys() |> Enum.count() > 0
  end

  defp payloadify_matches(state) do
    state |> Map.values() |> Enum.filter(&is_match_active(&1)) |> Enum.map(&get_match_payload(&1))
  end

  defp get_match_payload(match) do
    player_payload =
      Map.values(match.players)
      |> Enum.map(&get_player_payload(&1))
      |> Enum.reduce(%{}, fn p, acc -> put_in(acc, [p.name], p) end)

    put_in(match, [:players], player_payload)
    |> put_in([:timeLeft], match_time_left(match))
  end

  defp get_player_payload(player) do
    %{
      name: player.name,
      done: player.done,
      ready: player.ready,
      score: player.score,
      progress: player.progress,
      wpm: player.wpm,
      accuracy: player.accuracy,
      waiting: player.waiting,
      category: player.category
    }
  end

  @doc """
  Tick all matches
  """
  def tick_all_matches do
    matches = Agent.get(__MODULE__, &Map.values(&1))
    Enum.each(matches, &match_tick(&1.id))
  end

  @doc """
  Handle countdown for matches as well as match timeout
  """
  def match_tick(match_id) do
    match_status = get_match_status(match_id)
    handle_match_tick(match_id, match_status)
  end

  defp handle_match_tick(match_id, match_status = %{ready: true, countdown: 1, started: false}) do
    update_match(match_id, :quote, get_quote(match_id))
    update_match(match_id, :started, true)
    update_match(match_id, :start_time, Time.utc_now())
  end

  defp handle_match_tick(match_id, %{ready: true, started: false} = match_status) do
    update_match(match_id, :countdown, match_status.countdown - 1)
  end

  defp handle_match_tick(match_id, match_status) do
    players_left =
      Map.values(match_status.players)
      |> Enum.filter(fn p -> !p.done && !p.waiting end)
      |> Enum.count()

    if players_left === 0 do
      update_match(match_id, :done, true)
    end

    match = get_match_status(match_id)
    check_match_time(match_time_left(match), match_id)
  end

  defp check_match_time(0, match_id), do: update_match(match_id, :done, true)
  defp check_match_time(_time, _match_id), do: nil

  defp match_time_left(%{start_time: nil}), do: @match_limit

  defp match_time_left(match) do
    @match_limit - Time.diff(Time.utc_now(), match.start_time)
  end

  defp get_quote(match_id) do
    match = get_match_status(match_id)

    categories =
      match.players
      |> Map.values()
      |> Enum.reduce(%{}, &acc_quote_category(&2, &1))
      |> Map.to_list()

    if length(categories) == 0 do
      match.categories
      |> List.last()
      |> Quotes.get_quote()
    else
      categories
      |> Enum.sort_by(&elem(&1, 1))
      |> List.last()
      |> elem(0)
      |> String.to_atom()
      |> Quotes.get_quote()
    end
  end

  defp acc_quote_category(map, player) do
    if player.category != nil do
      if Map.has_key?(map, player.category) do
        put_in(map, [player.category], map[player.category] + 1)
      else
        put_in(map, [player.category], 1)
      end
    else
      map
    end
  end

  @doc """
  Resets a match
  """
  def reset_match(match_id) do
    match_status = get_match_status(match_id)

    if match_status.done do
      new_players =
        Map.keys(match_status.players)
        |> Enum.reduce(%{}, &put_in(&2, [&1], new_player(&1)))

      Agent.update(
        __MODULE__,
        &put_in(&1, [match_id], Map.merge(new_match(match_id), %{players: new_players}))
      )
    end
  end

  @doc """
  Create a new match map
  """
  def new_match(id) do
    %{
      id: id,
      players: %{},
      ready: false,
      started: false,
      countdown: @countdown,
      quote: "",
      start_time: nil,
      done: false,
      categories: Quotes.get_random_categories(3)
    }
  end

  @doc """
  Create a new player map
  """
  def new_player(name, waiting \\ false) do
    %{
      name: name,
      ready: false,
      typed: "",
      progress: 0,
      done: false,
      score: 0,
      wpm: 0,
      accuracy: 1,
      waiting: waiting,
      category: nil
    }
  end

  @doc """
  Update any value on a match
  """
  def update_match(match_id, field, value) do
    Agent.update(__MODULE__, &put_in(&1, [match_id, field], value))
  end

  @doc """
  Add a new player to the match
  """
  def put_player(match_id, name) do
    match_status = get_match_status(match_id)

    Agent.update(
      __MODULE__,
      &put_in(
        ensure_match(match_id, &1),
        [match_id, :players, name],
        new_player(name, match_status.started)
      )
    )

    check_players_ready(match_id)
    get_player(match_id, name)
  end

  @doc """
  Remove player from match
  """
  def remove_player(match_id, name) do
    Agent.update(__MODULE__, &(pop_in(&1, [match_id, :players, name]) |> elem(1)))
    check_players_ready(match_id)
    players(match_id)
  end

  @doc """
  Update any value on a player
  """
  def update_player(match_id, name, field, value) do
    Agent.update(__MODULE__, &put_in(&1, [match_id, :players, name, field], value))
  end

  defp does_match(quoteArray, letter, index) do
    if Enum.at(quoteArray, index) === letter, do: 1, else: 0
  end

  defp update_player_typed(match_id, name, typed) do
    match = get_match_status(match_id)
    player = get_player(match_id, name)

    if !player.done && match.started do
      update_player(match_id, name, :typed, typed)
      update_player(match_id, name, :progress, String.length(typed) / String.length(match.quote))
      elapsed_time = Time.diff(Time.utc_now(), match.start_time)
      words = String.length(typed) / 5
      wpm = words / (elapsed_time / 60)
      update_player(match_id, name, :wpm, wpm)

      quoteArray = String.graphemes(match.quote)
      typedArray = String.graphemes(typed)

      if Enum.count(typedArray) > 0 do
        correct =
          typedArray
          |> Enum.with_index()
          |> Enum.reduce(0, fn {letter, index}, acc ->
            acc + does_match(quoteArray, letter, index)
          end)

        accuracy = correct / Enum.count(typedArray)
        update_player(match_id, name, :accuracy, accuracy)
      end

      player_update_score(match_id, name)

      if String.length(typed) === String.length(match.quote) do
        player_done(match_id, name)
      end
    end
  end

  @doc """
  Inverts ready status of player
  """
  def player_ready(match_id, name) do
    access = [match_id, :players, name, :ready]
    Agent.update(__MODULE__, &put_in(&1, access, !get_in(&1, access)))
    check_players_ready(match_id)
    get_player(match_id, name)
  end

  defp player_done(match_id, name) do
    update_player(match_id, name, :done, true)
  end

  defp player_update_score(match_id, name) do
    match = get_match_status(match_id)
    player = get_player(match_id, name)
    score = player.accuracy * player.wpm
    update_player(match_id, name, :score, score)
  end

  @doc """
  Handle player input event
  """
  def player_input(match_id, name, input) do
    player = get_player(match_id, name)
    update_player_typed(match_id, name, String.trim_leading(input))
    get_player(match_id, name)
  end

  @doc """
  Handle player backspace event
  """
  def player_backspace(match_id, name) do
    player = get_player(match_id, name)

    if String.length(player.typed) > 0 do
      update_player_typed(
        match_id,
        name,
        String.slice(player.typed, 0, String.length(player.typed) - 1)
      )
    end

    get_player(match_id, name)
  end

  @doc """
  Cast vote for a category
  """
  def player_vote(match_id, name, category) do
    update_player(match_id, name, :category, category)
    get_player(match_id, name)
  end

  @doc """
  Check if all players in the match are ready
  """
  def check_players_ready(match_id) do
    match = get_match_status(match_id)
    players = match.players |> Map.values() |> Enum.filter(fn p -> !p.waiting end)
    player_count = Enum.count(players)

    cond do
      player_count > 0 && player_count < @player_ready_min &&
          Enum.all?(players, fn p -> p.ready end) ->
        match_ready(match_id)

      player_count >= @player_ready_min && players_ready(players) >= @player_ready_min ->
        match_ready(match_id)

      true ->
        match_not_ready(match_id)
    end
  end

  defp players_ready(players) do
    players |> Enum.filter(fn p -> p.ready end) |> Enum.count()
  end

  defp match_ready(match_id) do
    match = get_match_status(match_id)

    if !match.ready do
      update_match(match_id, :ready, true)
      update_match(match_id, :countdown, @countdown)
    end
  end

  defp match_not_ready(match_id) do
    update_match(match_id, :ready, false)
  end

  @doc """
  Get the status of a match
  """
  def get_match_status(match_id) do
    Agent.update(__MODULE__, &ensure_match(match_id, &1))
    Agent.get(__MODULE__, &get_in(&1, [match_id]))
  end

  @doc """
  Get a single player
  """
  def get_player(match_id, name) do
    Agent.get(__MODULE__, &get_in(&1, [match_id, :players, name]))
  end

  @doc """
  Get all players in the match
  """
  def players(match_id) do
    Agent.get(__MODULE__, &get_in(&1, [match_id, :players]))
  end

  @doc """
  Add the match id to the state if it doesn't exist
  """
  defp ensure_match(match_id, map) do
    if Map.has_key?(map, match_id) do
      map
    else
      Map.put_new(map, match_id, new_match(match_id))
    end
  end
end
