defmodule Typetype.Match do
  use Ecto.Schema
  import Ecto.Changeset


  schema "matches" do
    field :completed, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(match, attrs) do
    match
    |> cast(attrs, [:completed])
    |> validate_required([])
  end
end
