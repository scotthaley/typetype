defmodule Typetype.Repo do
  use Ecto.Repo,
    otp_app: :typetype,
    adapter: Ecto.Adapters.Postgres
end
