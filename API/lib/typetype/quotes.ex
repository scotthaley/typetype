defmodule Typetype.Quotes do
  
  def get_random_categories(count) do
    [
      :shakespeare, 
      :harrypotter,
      :lyrics,
      :movies,
      :poems
    ] |> Enum.take_random(count)
  end

  def get_quote(:shakespeare) do
    [
      "What a piece of work is man! How noble in reason! How infinite in faculty! In form and moving how express and admirable! In action how like an angel! In apprehension how like a god! The beauty of the world, the paragon of animals!",
      "All the world 's a stage, and all the men and women merely players. They have their exits and their entrances; And one man in his time plays many parts.",
      "The fool doth think he is wise, but the wise man knows himself to be a fool.",
      "If you prick us, do we not bleed? If you tickle us, do we not laugh? If you poison us, do we not die? And if you wrong us, shall we not revenge?",
      "Men at some time are masters of their fates: The fault, dear Brutus, is not in our stars, but in ourselves, that we are underlings.",
      "Cowards die many times before their deaths; The valiant never taste of death but once. Of all the wonders that I yet have heard, it seems to me most strange that men should fear; Seeing that death, a necessary end, will come when it will come",
      "Nothing in his life became him like the leaving it; he died as one that had been studied in his death to throw away the dearest thing he owed, as 'twere a careless trifle"
    ] |> Enum.random
  end
  def get_quote(:harrypotter) do
    [
      "I hope you're pleased with yourselves. We could all have been killed — or worse, expelled. Now if you don't mind, I'm going to bed.",
      "There will be no foolish wand-waving or silly incantations in this class. As such, I don't expect many of you to appreciate the subtle science and exact art that is potion-making. However, for those select few who possess the predisposition, I can teach you how to bewitch the mind and ensnare the senses. I can tell you how to bottle fame, brew glory, and even put a stopper in death. Then again, maybe some of you have come to Hogwarts in possession of abilities so formidable that you feel confident enough to not pay attention!",
      "It takes a great deal of bravery to stand up to our enemies, but just as much to stand up to our friends.",
      "But you know, happiness can be found even in the darkest of times, if one only remembers to turn on the light.",
      "I am what I am, an' I'm not ashamed. 'Never be ashamed,' my ol' dad used ter say, 'there's some who'll hold it against you, but they're not worth botherin' with.",
      "Give her hell from us, Peeves.' And Peeves, whom Harry had never seen take an order from a student before, swept his belled hat from his head and sprang to a salute as Fred and George wheeled about to tumultuous applause from the students below and sped out of the open front doors into the glorious sunset.",
      "The thing about growing up with Fred and George,' said Ginny thoughtfully, 'is that you sort of start thinking anything's possible if you've got enough nerve.'",
      "Words are, in my not-so-humble opinion, our most inexhaustible source of magic. Capable of both inflicting injury, and remedying it.",
      "It is a curious thing, Harry, but perhaps those who are best suited to power are those who have never sought it. Those who, like you, have leadership thrust upon them, and take up the mantle because they must, and find to their own surprise that they wear it well."
    ] |> Enum.random
  end
  def get_quote(:lyrics) do
    [
      "I can dig it, he can dig it, she can dig it, we can dig it, they can dig it, you can dig it, oh let's dig it. Can you dig it, baby?",
      "If Heaven and Hell decide that they both are satisfied, illuminate the No's on their Vacancy signs, if there's no one to guide you when your soul departs, then I will follow you into the dark.",
      "And I wonder when I sing along with you, if everything could ever feel this real forever, if anything could ever be this good again. The only thing I'll ever ask of you, you've got to promise not to stop when I say when. She sang.",
      "There are places I remember all my life, though some have changed. Some forever, not for better, some have gone and some remain.",
      "It ain't me, it ain't me, I ain't no Senator's son, son. It ain't me, it ain't me, I ain't no fortunate one.",
      "Rah, rah, ah, ah, ah, roma, roma, ma. Gaga, ooh, la, la... want your bad romance!",
      "There's nothing you can do that can't be done. Nothing you can sing that can't be sung. Nothing you can say but you can learn how to play the game. It's easy."
    ] |> Enum.random
  end
  def get_quote(:movies) do
    [
      "I'm somebody now, Harry. Everybody likes me. Soon, millions of people will see me and they'll all like me. I'll tell them about you, and your father, how good he was to us. It's a reason to smile. It makes tomorrow all right. Now when I get the sun, I smile.",
      "You're here because you know something. What you know you can't explain, but you feel it. You've felt it your entire life, that there's something wrong with the world. You don't know what it is, but it's there, like a splinter in your mind, driving you mad. You take the blue pill, the story ends, you wake up in your bed and believe whatever you want to believe. You take the red pill, you stay in Wonderland and I show you how deep the rabbit hole goes.",
      "I think if your clients want to sit on my shoulders and call themselves tall, they have the right to give it a try - but there's no requirement that I enjoy sitting here listening to people lie. You have part of my attention - you have the minimum amount. The rest of my attention is back at the offices of Facebook, where my colleagues and I are doing things that no one in this room, including and especially your clients, are intellectually or creatively capable of doing.",
      "He's the hero Gotham deserves, but not the one it needs right now. So we'll hunt him. Because he can take it. Because he's not our hero. He's a silent guardian, a watchful protector. A dark knight.",
      "I know what you're thinking. Did he fire six shots or only five? Well to tell you the truth, in all this excitement, I kind of lost track myself. But being that this is a .44 Magnum, the most powerful handgun in the world, and would blow your head clean off, you've got to ask yourself one question. 'Do I feel lucky?' Well do ya, punk?",
      "You talking to me? You talking to me? You talking to me? Well, then who the Hell else are you talking? You talking to me? Well, I'm the only one here.",
      "Mama always said life was like a box of chocolates. You never know what you're gonna get."
    ] |> Enum.random
  end
  def get_quote(:poems) do
    [
      "You may write me down in history with your bitter, twisted lies. You may tread me in the very dirt, but still, like dust, I'll rise.",
      "Twas brillig, and the slithy toves did gyre and gimble in the wabe: All mimsy were the borogoves, and the mome raths outgrabe. 'Beware the Jabberwock, my son! The jaws that bite, the claws that catch! Beware the Jubjub bird, and shun the frumious Bandersnatch!'",
      "\"Be that word our sign of parting, bird or fiend!\" I shrieked, upstarting - \"Get thee back into the tempest and the Night's Plutonian shore! Leave no black plume as a token of that lie thy soul hath spoken! Leave my loneliness unbroken! - quit the bust above my door! Take thy beak from out my heart, and take thy form from off my door!\" Quoth the Raven \"Nevermore.\"",
      "Beowulf answered, Ecgtheow's son: \"Grieve not, O wise one! For each it is better, his friend to avenge than with vehemence wail him; each of us must the end-day abide of his earthly existence; who is able accomplish glory ere death! To battle-thane noble lifeless lying, 'tis at last most fitting.",
      "I shall be telling this with a sigh, somewhere ages and ages hence: two roads diverged in a wood, and I - I took the one less traveled by, and that has made all the difference."
    ] |> Enum.random
  end
end