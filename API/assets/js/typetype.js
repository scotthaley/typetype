import { connectToSocket } from "./socket"
import { updateView } from "./liveviews"

const typetype = () => {
  console.log("TypeType Init!");

  let playerChannel;

  document.getElementById("join-btn").addEventListener("click", () => {
    const name = document.getElementById("name").value;
    const socket = connectToSocket(window.matchId, name);

    let htmlChannelMain = socket.channel(`html:${window.matchId}`, {})
    htmlChannelMain.join()
      .receive("ok", () => { console.log("Joined html channel successfully") })
      .receive("error", resp => { console.log("Unable to join html channel", resp) })

    htmlChannelMain.on("update", update => {
      updateView(update)
    });
    
    let htmlChannelPersonal = socket.channel(`html:${window.matchId}:${name}`, {})
    htmlChannelPersonal.join()
      .receive("ok", () => { console.log("Joined personal html channel successfully") })
      .receive("error", resp => { console.log("Unable to join html channel", resp) })

    htmlChannelPersonal.on("update", update => {
      updateView(update)
    });

    playerChannel = socket.channel("players:lobby", {})
    playerChannel.join()
      .receive("ok", resp => { console.log("Joined successfully", resp) })
      .receive("error", resp => { console.log("Unable to join", resp) })
  });

  const readyup = () => {
    playerChannel.push("player:ready");
  }

  return {
    readyup
  }
}

export default typetype;