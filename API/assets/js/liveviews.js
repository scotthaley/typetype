export const updateView = ({container, html}) => {
  document.querySelector(container).innerHTML = html;
}