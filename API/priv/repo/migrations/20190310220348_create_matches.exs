defmodule Typetype.Repo.Migrations.CreateMatches do
  use Ecto.Migration

  def change do
    create table(:matches) do
      add :completed, :boolean, default: false, null: false

      timestamps()
    end

  end
end
