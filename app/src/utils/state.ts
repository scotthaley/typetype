import { Socket, Channel } from "phoenix";

export interface IPlayer {
  name: string;
  ready: boolean;
  score: number;
  progress: number;
  wpm: number;
  accuracy: number;
  waiting: boolean;
  category: string;
}

export interface IPlayerSelf {
  name: string;
  ready: boolean;
  score: number;
  progress: number;
  wpm: number;
  accuracy: number;
  typed: string;
  waiting: boolean;
  category: string;
}

export interface IPlayerList {
  [name: string]: IPlayer;
}

export interface IMatch {
  id: number;
  players: IPlayerList;
  ready: boolean;
  started: boolean;
  countdown: number;
  quote: string;
  done: boolean;
  timeLeft: number;
  categories: string[];
}

export interface IRootState {
  matches: IMatch[];
  socket?: Socket;
  playerChannel?: Channel;
  joinedMatch: number | null;
  currentMatch?: IMatch;
  name?: string;
}

export const initialState: IRootState = {
  matches: [],
  joinedMatch: null
}
