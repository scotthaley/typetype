const theme = {
  default: {
    bg: "#2a2d34",
    color: {
      primary: "#30C5FF",
      secondary: "#5C946E",
      dark: "#2A2D34",
      primaryLight: "#A0DDE6",
      secondaryLight: "#80C2AF",
      primaryLightest: "#DCF2F5",
      black: "#000",
      white: "#fff",
      error: "#e04c4c",
      gray: "#999999"
    },
    font: {
      console: "monospace"
    },
    fontSize: {
      small: "14px",
      medium: "22px",
      large: "32px",
      huge: "32px"
    },
    space: [0, 4, 8, 16, 32, 48, 64]
  },
  jon: {
    bg: "#FFF",
    color: {
      primary: "#FF0000",
      secondary: "#FFFF00",
      dark: "#FFFFFF",
      primaryLight: "#FF0000",
      secondaryLight: "#FFFF00",
      primaryLightest: "#fa00ff",
      black: "#000",
      white: "#00ff15",
      error: "#0033ff",
      gray: "#00ff15"
    },
    font: {
      console: "Comic Sans MS"
    },
    fontSize: {
      small: "14px",
      medium: "22px",
      large: "32px",
      huge: "32px"
    },
    space: [0, 4, 8, 16, 32, 48, 64]
  }
};

export default theme;
