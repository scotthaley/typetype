import * as React from 'react';
import { IMatch } from 'src/utils/state';
import Box from 'components/atoms/Box';
import Card from 'components/molecules/Card';
import MatchRecord from 'components/molecules/MatchRecord';
import { History } from 'history';

interface IActiveMatchesProps {
  matches: IMatch[];
  history: History<any>
};

const ActiveMatches: React.FC<IActiveMatchesProps> = ({ matches, history }) => {

  const handleJoin = (id: number) => () => {
    history.push(`/match/${id}`);
  }

  return (
    <Box m={6} width={800}>
      <Card title="Active Matches">
        {matches.map(m =>
          <MatchRecord 
            key={m.id} 
            matchId={m.id} 
            players={m.players}
            onJoin={handleJoin(m.id)}
          />
        )}
      </Card>
    </Box>
  );
};

export default ActiveMatches;
