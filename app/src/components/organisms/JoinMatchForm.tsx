import * as React from 'react';
import Card from '../molecules/Card';
import Box from '../atoms/Box';
import Input from '../atoms/Input';
import Button from '../atoms/Button';

interface IJoinMatchForm {
  id: number;
  join: (name: string, matchId: number) => void;
}

const JoinMatchForm: React.FC<IJoinMatchForm> = ({ id, join }) => {
  const [name, setName] = React.useState('');

  const handleNameInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  }

  const handleJoin = () => {
    join(name, id);
  }

  const validName = (name: string) => name.length > 0 && name.length < 20;

  return (
    <Box m={6} width={600}>
      <Card title={`Join Match ${id}`}>
        <Box p={3} flex={true} alignItems="center">
          <Box flexGrow={true}>
            <Input placeholder="Who are you?" style={{ width: "100%" }} value={name} onChange={handleNameInput}/>
          </Box>
          <Box ml={4}>
            <Button onClick={handleJoin} disabled={!validName(name)}>Join</Button>
          </Box>
        </Box>
      </Card>
    </Box>
  );
}

export default JoinMatchForm
