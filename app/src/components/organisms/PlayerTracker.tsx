import * as React from 'react';
import { IPlayerList, IPlayerSelf } from 'src/utils/state';
import PlayerTrackerRow from '../molecules/PlayerTrackerRow';

interface IPlayerTrackerProps {
  players: IPlayerList;
  started: boolean;
  currentPlayer: IPlayerSelf;
}

const PlayerTracker: React.FC<IPlayerTrackerProps> = ({ currentPlayer, players, started }) => {
  const sortedPlayers = Object.values(players).sort((a, b) => {
    return b.score - a.score;
  });

  return (
    <>
      {sortedPlayers.map((p, i) => {
         const player = currentPlayer.name === p.name ? currentPlayer : p;
         return (
           <PlayerTrackerRow
             key={i}
             player={player}
             border={i !== 0}
             stats={started && !player.waiting}
             readyStatus={!started}
             currentPlayer={currentPlayer.name === p.name}
             spectating={player.waiting}
           />
         );
      })}
    </>
  );
}

export default PlayerTracker
