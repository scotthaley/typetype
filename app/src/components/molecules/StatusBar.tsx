import * as React from 'react';
import Box from '../atoms/Box';
import Text from '../atoms/Text';

interface IStatusBarProps {
  status: string;
}

const StatusBar: React.FC<IStatusBarProps> = ({ children, status }) => {
  return (
    <Box bg="primaryLightest" flex={true} alignItems="center">
      <Box p={2} pl={4} flexGrow={true}>
        <Text>{ status }</Text>
      </Box>
      <Box p={2} ml={3}>
        { children }
      </Box>
    </Box>
  );
}

export default StatusBar