import * as React from "react";
import Box from "../atoms/Box";
import { IPlayer } from "src/utils/state";
import Text from "../atoms/Text";
import theme from "utils/theme";
import { isAbsolute } from "path";

interface IPlayerTrackerRowProps {
  player: IPlayer;
  currentPlayer: boolean;
  stats: boolean;
  readyStatus: boolean;
  border?: boolean;
  spectating: boolean;
}

const PlayerTrackerRow: React.FC<IPlayerTrackerRowProps> = ({
  player,
  currentPlayer,
  border,
  stats,
  readyStatus,
  spectating
}) => {
  return (
    <Box
      bg="primaryLightest"
      height={60}
      style={{
        position: "relative",
        borderTop: border ? `2px solid ${theme.default.color.dark}` : "none"
      }}
    >
      <Box
        width={player.progress}
        bg={currentPlayer ? "secondary" : "primary"}
        style={{
          position: "absolute",
          top: "0",
          left: "0",
          bottom: "0",
          transition: "width 0.2s"
        }}
      />
      <Box
        flex={true}
        alignItems="stretch"
        style={{
          position: "absolute",
          top: "0",
          left: "0",
          bottom: "0",
          right: "0"
        }}
      >
        <Box px={3} flex={true} flexGrow={true} alignItems="center">
          <Text>{player.name}</Text>
        </Box>
        {stats && (
          <>
            <Box
              px={3}
              flex={true}
              alignItems="center"
              style={{ borderRight: `2px solid ${theme.default.color.dark}` }}
            >
              <Text>Score: {player.score.toFixed(2)}</Text>
            </Box>
            <Box
              px={3}
              flex={true}
              alignItems="center"
              style={{ borderRight: `2px solid ${theme.default.color.dark}` }}
            >
              <Text>WPM: {player.wpm.toFixed(2)}</Text>
            </Box>
            <Box
              px={3}
              flex={true}
              alignItems="center"
              style={{ borderRight: `2px solid ${theme.default.color.dark}` }}
            >
              <Text>Accuracy: {(player.accuracy * 100).toFixed(2)}%</Text>
            </Box>
            <Box px={3} flex={true} alignItems="center">
              <Text>{Math.round(player.progress * 100)}%</Text>
            </Box>
          </>
        )}
        {readyStatus && (
          <Box px={3} flex={true} alignItems="center">
            <Text>{player.ready ? "Ready" : "Not Ready"}</Text>
          </Box>
        )}
        {spectating && (
          <Box px={3} flex={true} alignItems="center">
            <Text>Spectating</Text>
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default PlayerTrackerRow;
