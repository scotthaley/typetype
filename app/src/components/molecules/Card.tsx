import * as React from 'react';
import Box from 'components/atoms/Box';
import Text from 'components/atoms/Text';

interface ICardProps {
  title: string;
};

const Card: React.FC<ICardProps> = ({
  title,
  children
}) => {
  return (
    <Box>
      <Box p={3} bg="secondaryLight">
        <Text>{title}</Text>
      </Box>
      <Box bg="primaryLight">
        {children}
      </Box>
    </Box>
  );
};

export default Card;
