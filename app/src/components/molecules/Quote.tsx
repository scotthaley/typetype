import * as React from 'react';
import Text from '../atoms/Text';

interface IQuoteProps {
  quote: string;
  typed: string;
}

const Quote: React.FC<IQuoteProps> = ({ quote, typed }) => {
  const quoteArray = quote.split('');
  const typedArray = typed.split('');

  const isCharCorrect = (i: number) => i >= typedArray.length || quoteArray[i] === typedArray[i];

  const charColor = (i: number) => {
    if (i >= typedArray.length) {
      return 'white';
    }
    if (quoteArray[i] === typedArray[i]) {
      return 'primary';
    } else {
      return 'error';
    }
  }

  const getChar = (i: number) => {
    if (i >= typedArray.length) {
      return quoteArray[i];
    }
    return typedArray[i];
  }

  return (
    <>
      {quoteArray.map((c, i) => {
        const char = getChar(i);
        const underscore = i === typedArray.length || (!isCharCorrect(i) && char === ' ');
        const whiteSpace = (char === ' ' && !isCharCorrect(i)) || char !== ' ' ? 'pre' : 'normal';
        return (
        <Text 
          key={i} 
          color={charColor(i)} 
          size="large" 
          underscore={underscore}
          style={{ whiteSpace }}
        >
          { char }
        </Text>
        ); 
      })}
    </>
  );
}

export default Quote