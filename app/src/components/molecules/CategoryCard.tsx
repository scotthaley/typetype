import * as React from 'react';
import Box from '../atoms/Box';
import styled from 'styled-components';
import Text from '../atoms/Text';
import { prop, theme } from 'styled-tools';

import harrypotter from 'categories/harrypotter.jpg';
import movies from 'categories/movies.jpg';
import music from 'categories/music.png';
import poems from 'categories/poems.jpg';
import shakespeare from 'categories/shakespeare.jpg';

interface ICategoryCardProps {
  name: string;
  selected: boolean;
  votes: number;
  onClick: () => void;
}

const ImageBox = styled.div<{ image: string }>`
  position: relative;
  height: 300px;
  width: 250px;
  background-image: url(${prop("image")});
  background-position: center;
  background-size: cover;
  cursor: pointer;
`;

const VoteCountBox = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
  width: 40px;
  height: 40px;
  border-radius: 50%;
  border: 2px solid ${theme("color.white")};
  background-color: ${theme("color.dark")};
  text-align: center;
`;

const CategoryCard: React.FC<ICategoryCardProps> = ({ name, onClick, selected, votes }) => {
  const getImage = (name: string) => {
    switch (name) {
      case "harrypotter": return harrypotter;
      case "movies": return movies;
      case "lyrics": return music;
      case "poems": return poems;
      case "shakespeare": return shakespeare;
      default: return "";
    }
  }

  const getTitle = (name: string) => {
    switch (name) {
      case "harrypotter": return "Harry Potter";
      case "movies": return "Movies";
      case "lyrics": return "Music";
      case "poems": return "Poems";
      case "shakespeare": return "Shakespeare";
      default: return "";
    }
  }

  return (
    <Box flex={true} flexDirection="column" onClick={onClick}>
      <ImageBox image={getImage(name)}>
        {votes > 0 &&
        <VoteCountBox>
          <Text color="primary" size="large">{votes}</Text>
        </VoteCountBox>}
      </ImageBox>
      <Box p={3} bg={selected ? 'primary' : 'primaryLightest'} style={{ textAlign: 'center' }}>
        <Text>{ getTitle(name) }</Text>
      </Box>
    </Box>
  );
}

export default CategoryCard