import * as React from "react";
import styled from "styled-components";
import Logo from "components/atoms/Logo";
import Box from "components/atoms/Box";
import Text from "../atoms/Text";

const NavBarContainer = styled(Box)`
  height: 50px;
  background-color: ${props => props.theme.color.primary};
`;

interface INavBar {
  joinedMatch: number | null;
  changeTheme: () => void;
}

const NavBar: React.FC<INavBar> = ({ joinedMatch, changeTheme }) => {
  return (
    <NavBarContainer p={1} pl={4} flex={true} alignItems="center">
      <Box flex={true} flexGrow={true} alignItems="center">
        <Box flexGrow={true}>
          <Logo />
        </Box>
        <Box onClick={changeTheme} pr={4}>
          <Text>Toggle Theme</Text>
        </Box>
        {joinedMatch && (
          <Box flex={true} alignItems="center" px={2}>
            <Text>Joined Match: {joinedMatch}</Text>
          </Box>
        )}
      </Box>
    </NavBarContainer>
  );
};

export default NavBar;
