import Box from 'components/atoms/Box';
import Text from 'components/atoms/Text';
import * as React from 'react';
import { IPlayerList } from 'src/utils/state';
import Button from 'components/atoms/Button';

interface IMatchRecordProps {
  matchId: number;
  players: IPlayerList;
  playerDisplayCount?: number;
  onJoin: () => void;
};

const playerText: (players: IPlayerList, count: number) => string = (players, count) => {
  const playerArray = Object.values(players);
  const diff = playerArray.length - count;
  return playerArray.splice(0, count).map(p => p.name).join(', ') + (diff > 0 ? ` and ${diff} more` : '');
};

const MatchRecord: React.FC<IMatchRecordProps> = ({
  matchId,
  players,
  playerDisplayCount = 3,
  onJoin
}) => {
  const [isHovering, setIsHovering] = React.useState(false);

  const handleHover = (hover: boolean) => () => setIsHovering(hover);

  return (
    <Box
      p={3}
      flex={true}
      alignItems="center"
      onMouseEnter={handleHover(true)}
      onMouseLeave={handleHover(false)}
      height={20}
    >
      <Box flexGrow={true}>
        <Text>Match {matchId}</Text>
      </Box>
      <Box>
        {isHovering &&
        <Button onClick={onJoin}>Join</Button>}
        {!isHovering &&
        <Text>{playerText(players, playerDisplayCount)}</Text>}
      </Box>
    </Box>
  );
};

export default MatchRecord;
