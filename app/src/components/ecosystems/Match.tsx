import * as React from 'react';
import { ITypetypeActions } from 'src/App';
import { IMatch, IPlayerSelf } from 'src/utils/state';
import Box from '../atoms/Box';
import Button from '../atoms/Button';
import Quote from '../molecules/Quote';
import StatusBar from '../molecules/StatusBar';
import PlayerTracker from '../organisms/PlayerTracker';
import CategoryCard from '../molecules/CategoryCard';

interface IMatchProps {
  self: IPlayerSelf;
  match: IMatch;
  actions: ITypetypeActions;
  typed: string;
}

const Match: React.FC<IMatchProps> = ({ actions, match, self, typed }) => {
  const status = match.done
    ? 'Match finished'
    : match.started 
      ? `Type now! ${match.timeLeft}s left` 
      : match.ready 
        ? `Match starting in ${match.countdown}...` 
        : 'Waiting for everybody to ready up';

  React.useEffect(() => {
    document.addEventListener('keydown', (e) => {
      if (e.keyCode === 8) {
        actions.playerBackspace();
      }
      if (e.key.length === 1) {
        actions.playerInput(e.key);
      }
    });
  }, []);

  const handleVote = (name: string) => () => actions.categoryVote(name);
  const getVotes = (name: string) => Object.values(match.players).filter(p => p.category === name).length;

  return (
    <Box flex={true} flexDirection="column" flexGrow={true}>
      <StatusBar status={status}>
        {!match.started && !match.done &&
        <Button onClick={actions.ready}>{ self.ready ? "I'm Not Ready" : "I'm Ready" }</Button>}
        {match.done &&
         <Button onClick={actions.resetMatch}>Race Again</Button>}
      </StatusBar>
      {!match.started &&
      <Box m={4} flex={true} justifyContent="space-around">
        {match.categories.map((c, i) => 
          <CategoryCard key={i} name={c} onClick={handleVote(c)} selected={self.category === c} votes={getVotes(c)}/>)}
      </Box>}
      {match.started &&
      <Box m={5}>
        <Quote quote={match.quote} typed={ typed } />
      </Box>}
      <Box m={5} flexGrow={true} style={{ overflow: "auto" }}>
        <PlayerTracker currentPlayer={self} players={match.players} started={match.started}/>
      </Box>
    </Box>
  );
}

export default Match
