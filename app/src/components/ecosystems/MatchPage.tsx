import Box from 'components/atoms/Box';
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { ITypetypeActions } from 'src/App';
import { IMatch, IPlayerSelf } from 'src/utils/state';
import JoinMatchForm from '../organisms/JoinMatchForm';
import Match from './Match';

interface IMatchPage extends RouteComponentProps<{id: string}> {
  actions: ITypetypeActions;
  currentMatch?: IMatch;
  self?: IPlayerSelf;
  typed: string;
}

const MatchPage: React.FC<IMatchPage> = ({ actions, currentMatch, match, self, typed }) => {
  return (
    <Box flex={true} justifyContent="center" flexGrow={true}>
      {!currentMatch && 
      <JoinMatchForm 
        id={parseInt(match.params.id)}
        join={actions.joinMatch}
      />}
      {currentMatch && self &&
      <Match match={currentMatch} actions={actions} self={self} typed={typed}/>}
    </Box>
  );
}

export default MatchPage
