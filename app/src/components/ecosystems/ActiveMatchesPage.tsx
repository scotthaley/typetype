import Box from 'components/atoms/Box';
import ActiveMatches from 'components/organisms/ActiveMatches';
import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { IMatch } from 'utils/state';

interface IActiveMatchesPage extends RouteComponentProps {
  matches: IMatch[]
};

const ActiveMatchesPage: React.FC<IActiveMatchesPage> = ({ matches, history }) => {
  return (
    <Box flex={true} justifyContent="center">
      <ActiveMatches matches={matches} history={history}/>
    </Box>
  );
};

export default ActiveMatchesPage;
