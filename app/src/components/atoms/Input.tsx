import styled from 'styled-components';
import { theme } from 'styled-tools';

const Input = styled.input`
  padding: ${theme("space.1")}px ${theme("space.2")}px;
  font-family: ${theme("font.console")};
  font-size: ${theme("fontSize.medium")};
  background: ${theme("color.primaryLightest")};
  border: 2px solid ${theme("color.dark")};
  border-radius: 4px;
`

export default Input