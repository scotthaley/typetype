import styled from 'styled-components';
import { theme } from 'styled-tools';

const Button = styled.button`
  background: none;
  color: ${theme("color.dark")};
  border: 2px solid ${theme("color.dark")};
  border-radius: 3px;
  font-family: ${theme("font.console")};
  font-size: ${theme("fontSize.medium")};
  padding: ${theme("space.1")}px ${theme("space.4")}px;
  cursor: pointer;

  &:hover {
    background-color: ${theme("color.primary")};
  }

  &:disabled {
    background-color: ${theme("color.gray")};
  }
`;

export default Button;
