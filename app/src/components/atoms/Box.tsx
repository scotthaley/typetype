import * as React from "react";
import styled from "styled-components";
import { ifProp, prop, withProp } from "styled-tools";

interface IBoxProps {
  bg?: string;
  p?: number;
  pt?: number;
  pl?: number;
  pr?: number;
  pb?: number;
  px?: number;
  py?: number;
  m?: number;
  mt?: number;
  ml?: number;
  mr?: number;
  mb?: number;
  mx?: number;
  my?: number;
  flex?: boolean;
  flexDirection?: string;
  flexGrow?: boolean;
  alignItems?: string;
  justifyContent?: string;
  width?: number;
  height?: number;
  onMouseEnter?: any;
  onMouseLeave?: any;
  onClick?: any;
  style?: any;
  className?: string;
}

const createWidthProperty = (width: number) =>
  width > 1 ? `${width}px` : `${width * 100}%`;

const StyledBox = styled.div<IBoxProps>`
  padding: ${props =>
    props.p ? `${props.theme.space[props.p]}px` : undefined};
  padding-right: ${props =>
    props.pr ? `${props.theme.space[props.pr]}px` : undefined};
  padding-right: ${props =>
    props.px ? `${props.theme.space[props.px]}px` : undefined};
  padding-left: ${props =>
    props.pl ? `${props.theme.space[props.pl]}px` : undefined};
  padding-left: ${props =>
    props.px ? `${props.theme.space[props.px]}px` : undefined};
  padding-bottom: ${props =>
    props.pb ? `${props.theme.space[props.pb]}px` : undefined};
  padding-bottom: ${props =>
    props.py ? `${props.theme.space[props.py]}px` : undefined};
  padding-top: ${props =>
    props.pt ? `${props.theme.space[props.pt]}px` : undefined};
  padding-top: ${props =>
    props.py ? `${props.theme.space[props.py]}px` : undefined};
  margin: ${props => (props.m ? `${props.theme.space[props.m]}px` : undefined)};
  margin-right: ${props =>
    props.mr ? `${props.theme.space[props.mr]}px` : undefined};
  margin-right: ${props =>
    props.mx ? `${props.theme.space[props.mx]}px` : undefined};
  margin-left: ${props =>
    props.ml ? `${props.theme.space[props.ml]}px` : undefined};
  margin-left: ${props =>
    props.mx ? `${props.theme.space[props.mx]}px` : undefined};
  margin-bottom: ${props =>
    props.mb ? `${props.theme.space[props.mb]}px` : undefined};
  margin-bottom: ${props =>
    props.my ? `${props.theme.space[props.my]}px` : undefined};
  margin-top: ${props =>
    props.mt ? `${props.theme.space[props.mt]}px` : undefined};
  margin-top: ${props =>
    props.my ? `${props.theme.space[props.my]}px` : undefined};

  display: ${ifProp("flex", "flex")};
  align-items: ${prop("alignItems")};
  justify-content: ${prop("justifyContent")};
  flex-direction: ${prop("flexDirection")};
  flex-grow: ${ifProp("flexGrow", "1")};

  background-color: ${props =>
    props.bg ? props.theme.color[props.bg] : undefined};
  width: ${ifProp(
    "width",
    withProp("width", width => createWidthProperty(width))
  )};
  height: ${ifProp("height", withProp("height", height => `${height}px`))};
`;

const Box: React.FC<IBoxProps> = props => {
  return <StyledBox {...props}>{props.children}</StyledBox>;
};

export default Box;
