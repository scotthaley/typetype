import * as React from 'react';
import Text from 'components/atoms/Text';

const fullLogoText = 'typetype';

const Logo: React.FC = () => {
  const [logoText, setLogoText] = React.useState('');

  const updateLogoText = (text: string, blink: number) => {
    const newText = fullLogoText.substr(0, text.length + 1);
    const showCursor = newText !== fullLogoText || blink < 5;
    setLogoText(`${newText}${showCursor ? '_' : ''}`);
    setTimeout(() => { updateLogoText(newText, blink < 10 ? blink + 1 : 0) }, 100);
  }

  React.useEffect(() => {
    updateLogoText('', 0);
  }, [])

  return (
    <Text color="dark" size="large">{ logoText }</Text>
  );
};

export default Logo;