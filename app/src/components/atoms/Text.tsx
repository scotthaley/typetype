import * as React from 'react';
import styled from 'styled-components';
import { ifProp } from 'styled-tools';

interface ITextProps {
  color?: string;
  size?: string;
  underscore?: boolean;
  style?: any;
}

const TextSpan = styled.span<ITextProps>`
  font-family: ${props => props.theme.font.console};
  font-size: ${props => props.theme.fontSize[props.size || "medium"]};
  color: ${props => props.theme.color[props.color || "black"]};
  text-decoration: ${ifProp("underscore", "underline")};
`;

const Text: React.FC<ITextProps> = (props) => {
  return (
    <TextSpan {...props}>{props.children}</TextSpan>
  );
}

export default Text