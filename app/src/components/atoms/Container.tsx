import styled from "styled-components";
import { theme } from "styled-tools";

const Container = styled.div`
  height: 100%;
  background-color: ${theme("bg")};
`;

export default Container;
