import { fetchActiveMatches } from "api/matches";
import ActiveMatchesPage from "components/ecosystems/ActiveMatchesPage";
import MatchPage from "components/ecosystems/MatchPage";
import NavBar from "components/molecules/NavBar";
import { createBrowserHistory } from "history";
import { Socket } from "phoenix";
import * as React from "react";
import { Route, Router } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import { IMatch, initialState, IPlayerSelf } from "utils/state";
import theme from "utils/theme";
import "./App.css";
import Box from "./components/atoms/Box";
import Container from "./components/atoms/Container";

const history = createBrowserHistory();

export interface ITypetypeActions {
  joinMatch: (name: string, matchId: number) => void;
  ready: () => void;
  playerInput: (key: string) => void;
  playerBackspace: () => void;
  resetMatch: () => void;
  categoryVote: (name: string) => void;
}

const App: React.FC = () => {
  const [state, setState] = React.useState(initialState);
  const [currentMatch, setCurrentMatch] = React.useState<IMatch | undefined>(
    undefined
  );
  const [self, setSelf] = React.useState<IPlayerSelf | undefined>(undefined);
  const [typed, setTyped] = React.useState("");
  const [themed, setThemed] = React.useState(theme.default);

  React.useEffect(() => {
    loadActiveMatches();
  }, []);

  const loadActiveMatches = () => {
    fetchActiveMatches()
      .then(response =>
        setState({
          ...state,
          matches: response.matches
        })
      )
      .catch(console.log);
  };

  const actions: ITypetypeActions = {
    joinMatch: (name, matchId) => {
      const socket = new Socket(
        process.env.REACT_APP_WS_ENDPOINT || "ws://localhost:4000/socket",
        { params: { matchId, name } }
      );
      socket.connect();

      const playerChannel = socket.channel(`players:${matchId}`, {});
      playerChannel
        .join()
        .receive("ok", () => {
          playerChannel.on("match_status", resp => {
            setCurrentMatch(resp);
          });
          playerChannel.on("self_update", (resp: IPlayerSelf) => {
            setSelf(resp);
          });
          playerChannel.on("match_reset", () => {
            setTyped("");
            playerChannel.push("player:fetch", {});
          });

          setState({
            ...state,
            socket,
            playerChannel,
            joinedMatch: matchId,
            name
          });
        })
        .receive("error", resp => {
          console.log("Unable to join", resp);
        });
    },
    ready: () => {
      if (state.playerChannel) {
        state.playerChannel.push("player:ready", {});
      }
    },
    playerInput: key => {
        setTyped(t => {
          if (state.playerChannel) {
            state.playerChannel.push("player:input", { input: t + key });
          }
          return t + key
        });
    },
    playerBackspace: () => {
      if (state.playerChannel) {
        setTyped(t => t.substring(0, t.length - 1));
        state.playerChannel.push("player:backspace", {});
      }
    },
    resetMatch: () => {
      if (state.playerChannel) {
        state.playerChannel.push("player:reset_match", {});
      }
    },
    categoryVote: name => {
      if (state.playerChannel) {
        state.playerChannel.push("player:category_vote", { name });
      }
    }
  };

  const handleChangeTheme = () => {
    setThemed(theme.jon);
  };
  return (
    <ThemeProvider theme={themed}>
      <Container>
        <Box className="App" flex={true} flexDirection="column">
          <NavBar
            joinedMatch={state.joinedMatch}
            changeTheme={handleChangeTheme}
          />
          <Router history={history}>
            <Route
              path="/"
              exact
              render={props => (
                <ActiveMatchesPage matches={state.matches} {...props} />
              )}
            />
            <Route
              path="/match/:id"
              render={props => (
                <MatchPage
                  actions={actions}
                  currentMatch={currentMatch}
                  self={self}
                  typed={typed}
                  {...props}
                />
              )}
            />
          </Router>
        </Box>
      </Container>
    </ThemeProvider>
  );
};

export default App;
