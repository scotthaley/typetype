export const fetchActiveMatches = async () => {
  const response = await fetch(process.env.REACT_APP_API_ENDPOINT + "/matches");
  return response.json();
};
